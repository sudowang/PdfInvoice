import React from 'react';
import { Steps } from '@douyinfe/semi-ui';
import { Location, useLocation, useNavigate } from "react-router-dom";

export default () => {
    const location = useLocation();
    const indexed = ['/upload', '/download']
    const navigate = useNavigate();

    function getIndex(location: Location) {
        const index = indexed.indexOf(location.pathname);
        if (index !== -1) return index;
        return 0;
    }

    function goTo(i: number) {
        if (i != 0) return;
        const href = indexed[i];
        navigate(href, { state: location.state });
    }

    const index = getIndex(location);
    return <>
        <h2 style={{ margin: '12px 0 15px 0' }} >PDF电子发票识别工具</h2 >
        <Steps type='basic' current={index} onChange={goTo} >
            <Steps.Step title="上传发票" description="批量上传你的PDF发票文件" />
            <Steps.Step title="下载结果" description="下载识别的发票号码和金额，并且打印" />
        </Steps >
    </>

}


export default class PdfInvoiceResult {
    success: boolean
    fileName: string
    message: string
    invoice: Invoice
    selected: boolean = true
}

export class Invoice {
    base64Png: string = ''
    title: string
    machineNumber: string
    code: string
    number: string
    date: string
    checksum: string
    buyerName: string
    buyerCode: string
    buyerAddress: string
    buyerAccount: string
    password: string
    amount: number
    taxAmount: number
    totalAmountString: string
    totalAmount: number
    sellerName: string
    sellerCode: string
    sellerAddress: string
    sellerAccount: string
    payee: string
    reviewer: string
    drawer: string
    type: string
    detailList: DetailList[]

    invoiceType(): string {
        if (!this.number) return 'ElectronicPlainInvoice'
        if (this.code && this.number) return 'PlainInvoice';
        return 'ElectronicSpecialInvoice'
    }
}

interface DetailList {
    name: string
    model: string
    unit: string
    count: number
    price: number
    amount: number
    taxRate: number
    taxAmount: number
}
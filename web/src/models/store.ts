import { FileItem } from "@douyinfe/semi-ui/lib/es/upload";
import PdfInvoiceResult from "./PdfInvoiceResult.ts";

interface Store {
    files: FileItem[],
    pdfResults: PdfInvoiceResult[],
    pdfSuccessResults: PdfInvoiceResult[],
}

export default {
    files: [],
    get pdfResults(): PdfInvoiceResult[] {
        return this.files.filter(x => x.response).map(file => (file.response as PdfInvoiceResult))
    },
    get pdfSuccessResults(): PdfInvoiceResult[] {
        return this.pdfResults.filter(r => r?.success);
    }
} as Store;
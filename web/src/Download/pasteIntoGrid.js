function run() {
    function findFrame(frame, keyword) {
        // 检查当前 frame 的 URL 是否包含关键字
        if (frame.location.href.indexOf(keyword) >= 0) {
            return frame;
        }

        // 遍历当前 frame 的子 frame
        for (var i = 0; i < frame.frames.length; i++) {
            var childFrame = frame.frames[i];

            // 递归查找子 frame
            var result = findFrame(childFrame, keyword);
            if (result) {
                return result;
            }
        }
        // 如果未找到目标 frame，则返回 null
        return null;
    }

    cd(window.top);

    var targetFrame = findFrame(window, 'Invoice_AddAndUse');
    if (!targetFrame) {
        return;
    }
    cd(targetFrame)
    var rowIds= []
    function renderRow(index, type, code, number, total) {
        var id = rowIds[index]
        mygrid.cells(id, 1).setValue(type);  //ElectronicPlainInvoice  ElectronicSpecialInvoice
        mygrid.cells(id, 2).setValue(code);
        mygrid.cells(id, 3).setValue(number);
        mygrid.cells(id, 4).setValue(total);
    }

    function addRows(count) {
        if (count <= 5) return;
        for (var i = 0; i < count - 5; i++) {
            AddFileGrid()
        }
    }
    mygrid.forEachRow(function (id) {
        rowIds.push(id)
    })

}


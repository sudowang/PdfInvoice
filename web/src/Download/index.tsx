import { useNavigate } from 'react-router-dom'
import PdfInvoiceResult from "../models/PdfInvoiceResult.ts";
import React, { useEffect, useState } from "react";
import InvoicesTable from "./InvoicesTable.tsx";
import store from "../models/store.ts";
import { Button } from "@douyinfe/semi-ui";
import { IconCopy, IconEyeOpened, IconFile } from "@douyinfe/semi-icons";
import PrintView from "./PrintView.tsx";
import './index.css'
import downloadExcel from "./downloadExcel.tsx";
import CopyCodeModal from "./CopyCodeModal.tsx";

export default function DownloadFc() {
    const navigate = useNavigate();
    const pdfResults = store?.pdfSuccessResults as PdfInvoiceResult[];
    const [openPrintView, setOpenPrintView] = useState(false);
    const [openCopyCode, setOpenCopyCode] = useState(false);
    useEffect(() => {
        if (!pdfResults?.length) {
            navigate('/upload')
            return;
        }
    }, [pdfResults])


    if (!pdfResults?.length) {
        return null;
    }

    return <>
        <div>
            <Button
                icon={<IconFile />}
                onClick={downloadExcel}
                className="action-button"
                size={'large'}
                type="primary"
                theme="solid"
            >下载Excel</Button>
            &nbsp;
            <Button
                icon={<IconEyeOpened />}
                onClick={() => setOpenPrintView(true)}
                className="action-button"
                size={'large'}
                type="warning"
                theme="solid"
            >打印预览</Button>
            &nbsp;
            <Button
                icon={<IconCopy />}
                onClick={() => setOpenCopyCode(true)}
                className="action-button"
                size={'large'}
                type="secondary"
                theme="solid"
            >内网一键填表代码</Button>
        </div>
        <PrintView visible={openPrintView} setVisible={setOpenPrintView} />
        <CopyCodeModal setVisible={setOpenCopyCode} visible={openCopyCode} />
        <InvoicesTable pdfInvoices={store.pdfResults} />
        <br />
    </>
}
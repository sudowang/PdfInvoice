import { Button, SideSheet } from "@douyinfe/semi-ui";
import store from "../models/store.ts";
import PrintViewPage from "./PrintViewPage.tsx";
import { IconPrint } from "@douyinfe/semi-icons";
import printJS from 'print-js';
import printCss from './printCss.ts';

export default function PrintView({ visible, setVisible }: { visible: boolean, setVisible: (b: boolean) => void }) {
    const invoices = store.pdfSuccessResults.filter(x => x.selected)
    const invoicePage = []
    for (let i = 0; i < invoices.length; i += 2) {
        invoicePage.push(invoices.slice(i, i + 2));
    }

    function startPrint() {
        printJS({
            printable: 'print-container',
            style: printCss,
            type: 'html',
            scanStyles: false,
        })
    }

    return <SideSheet title="打印凭证"
                      width="235mm"
                      visible={visible}
                      onCancel={() => setVisible(false)}>
        <Button
            icon={<IconPrint />}
            onClick={startPrint}
            style={{ width: '120px', marginLeft: '17px', marginBottom: '10px', height: '40px' }}
            size={'large'}
            type="tertiary"
            theme="solid"
        >开始打印</Button>
        <div className="print-view">
            <div id={"print-container"}>
                {invoicePage.map(pagePdfs => <PrintViewPage pdfInvoices={pagePdfs} key={pagePdfs[0].fileName} />)}
            </div>
        </div>
    </SideSheet>
}
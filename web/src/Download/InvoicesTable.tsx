import PdfInvoiceResult from "../models/PdfInvoiceResult.ts";
import { ColumnProps, RowSelection } from "@douyinfe/semi-ui/lib/es/table";
import { Checkbox, Table, Tag, Typography } from "@douyinfe/semi-ui";
import { useState } from 'react'


export default ({ pdfInvoices }: { pdfInvoices: PdfInvoiceResult[] }) => {
    const [sum, setSum] = useState(callSum());

    function callSum(): string {
        return pdfInvoices.filter(x => x.selected).reduce((a, b) => a + b.invoice.totalAmount, 0).toFixed(2) + "￥"
    }

    const columnGroups: ColumnProps<PdfInvoiceResult> = [
        {
            title: '选择',
            render: (_: any, pdf: PdfInvoiceResult) => {
                return <Checkbox defaultChecked={pdf.selected} onChange={_ => {
                    pdf.selected = !pdf.selected;
                    setSum(callSum())
                }} />
            }
        },
        {
            title: '序号',
            align: 'center' as const,
            render: (_: any, __: any, index: number) => {
                return index + 1
            }
        },
        {
            title: '文件名',
            align: 'center' as const,
            dataIndex: 'fileName'
        },
        {
            title: '处理状态',
            align: 'center' as const,
            render: (_: any, result: PdfInvoiceResult) => {
                if (result.success) {
                    return <Tag color="green" type="solid">处理成功 </Tag>
                }
                return <Tag color="pink">处理失败 </Tag>
            }
        },
        {
            title: '发票代码',
            align: 'center' as const,
            render: (_: any, record: PdfInvoiceResult) => {
                if (!record.success) return null;
                return record.invoice?.code;
            }
        },
        {
            title: '发票号码',
            align: 'center' as const,
            render: (_: any, record: PdfInvoiceResult) => {
                if (!record.success) return null;
                return record.invoice?.number;
            }
        },
        {
            title: '金额',
            align: 'center' as const,
            render: (_: any, record: PdfInvoiceResult) => {
                if (!record.success) return null;
                return record.invoice?.totalAmount?.toFixed(2);
            }
        }
    ];
    return <>
        <h4 style={{ marginBottom: '8px', marginTop: '0' }}>{sum}</h4>
        <Table id="current-weather-table"
               rowKey={r => r!.fileName}
               columns={columnGroups as any} dataSource={pdfInvoices} pagination={false} bordered={true} />
    </>

}
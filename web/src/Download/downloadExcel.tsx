import * as XLSX from 'xlsx';
import store from "../models/store.ts";

export default function () {
    const rows = store.pdfSuccessResults.filter(x => x.selected).map(pdfResult => {
        return [
            pdfResult.fileName,
            pdfResult.invoice.date,
            pdfResult.invoice.code,
            pdfResult.invoice.number,
            pdfResult.invoice.totalAmount
        ]
    });
    if (!rows.length) return;
    const headers = ['文件名', '发票时间', '发票代码', '发票号码', '总金额'];
    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.aoa_to_sheet([headers, ...rows]);
    // 为 totalAmount 列设置数字格式
    XLSX.utils.book_append_sheet(workbook, worksheet, "发票下载");
    worksheet['!cols'] = [{ wch: 35 }, { wch: 15 }, { wch: 15 }, { wch: 12 }, { wch: 10 }, { wch: 40 }];
    const fileName = "发票下载.xlsx";
    XLSX.writeFile(workbook, fileName);
}
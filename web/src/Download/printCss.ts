// 实际打印的css设置
export default `

@media print {
    @page {
        size: A4 portrait; /* auto is default portrait; */
        margin:10mm !important;
    }
}

body {
    margin: 0;
    padding: 0;
}

#print-container {
    margin: 0 auto;
    width: 210mm;
}

#print-container .print-view-page {
    margin: 0 auto;
    width: 210mm;
    height: 297mm;
    background-color: #fff;
    display: block;
    page-break-after: always;
}


.print-view-page img, .print-view-page .empty {
    object-fit: contain;
    display: block;
    width: 100%;
    height: 152mm;
}

.print-view-page .clipper {
    width: 100%;
    height: 0;
    border-bottom: 1mm dashed #aaa;
}
`
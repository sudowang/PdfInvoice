import PdfInvoiceResult from "../models/PdfInvoiceResult.ts";

/** 每一个打印页，2个发票组成一个打印页 */
export default function PrintViewPage({ pdfInvoices }: { pdfInvoices: PdfInvoiceResult[] }) {
    return <div className="print-view-page" >
        <img alt={pdfInvoices[0].fileName} src={pdfInvoices[0].invoice.base64Png} />
        <div className="clipper" ></div >
        {pdfInvoices.length > 1 ? <img alt={pdfInvoices[1].fileName} src={pdfInvoices[1].invoice.base64Png} /> : <div className="empty" />}
    </div >
}
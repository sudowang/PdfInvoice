import {Navigate, Route, Routes} from 'react-router-dom'
import { BrowserRouter } from "react-router-dom";
import Header from './Header'
import Upload from "./Upload";
import Download from "./Download";

function App() {
    return <BrowserRouter>
        <Header/>
        <br/>
        <Routes>
            <Route path="/upload" element={<Upload/>}/>
            <Route path="/download" element={< Download/>}/>
            <Route path="*" element={<Navigate to={"/upload"}/>}/>
        </Routes>
    </BrowserRouter>
}

export default App

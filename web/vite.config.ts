import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    server: {
        proxy: {
            '/invoice': {
                target: 'http://localhost:5477',
                changeOrigin: true,
            }
        }
    },
    build: {
        outDir: './../server/src/main/resources/static'
    }
})

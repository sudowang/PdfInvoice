package einvoice;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.sgidi.einvoice.models.Invoice;
import com.sgidi.einvoice.service.PdfQrExtractor;

public class Test {

    public static void main(String[] args) {
        try {
            String directoryPath = "d:\\invoice";

            // 获取文件夹中所有PDF文件的路径
            List<File> pdfFiles = Files.walk(Paths.get(directoryPath))
                    .filter(Files::isRegularFile)
                    .filter(path -> path.toString().toLowerCase().endsWith(".pdf"))
                    .map(java.nio.file.Path::toFile)
                    .collect(Collectors.toList());
            for (File file : pdfFiles) {
                Invoice in = PdfQrExtractor.extract(file);
                System.out.println(in.getCode() + " " + in.getNumber() + " " + in.getTotalAmount());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

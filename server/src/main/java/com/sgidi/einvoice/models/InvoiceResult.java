package com.sgidi.einvoice.models;

public class InvoiceResult {
    private String fileName;
    private boolean success = true;
    private String message = "";
    private Invoice invoice;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.success = false; // 当设置了message时，success为false，因为默认情况下，message是null，success是true
        this.message = message;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}

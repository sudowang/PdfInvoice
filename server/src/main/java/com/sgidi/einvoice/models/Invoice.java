package com.sgidi.einvoice.models;

import java.math.BigDecimal;
import java.util.List;

public class Invoice {
    private String base64Png;
    private String code;
    private String number;
    private BigDecimal totalAmount;

    // 是否是电子发票（2023年已经开始实施的新型发票，没有代码（code）只有号码(number)
    public boolean isElectronic() {
        return code == null || code.isEmpty();
    }


    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        if (this.number != null) return; // 正则会匹配到多次，这里我做个一次性赋值，不要重复搞了
        this.number = number;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getBase64Png() {
        return base64Png;
    }

    public void setBase64Png(String base64Png) {
        this.base64Png = base64Png;
    }
}

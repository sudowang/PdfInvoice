# 文件夹介绍

## 文件结构
+ server是后端，java的springboot程序，使用maven打包
+ web是前端，使用pnpm+vite


# 前端开发
+ 直接运行server程序，会在`5477`端口运行
+ [第一次开发]，请运行 `pnpm install`
+ 在web文件夹，使用`pnpm run start`开启前端开发模式，程序已经自动做好反代，不需要担心`5477`端口的问题


# 发布
+ web里运行`pnpm run build`，程序会打包到 `server\src\main\resources\static`
+ 在server里运行maven package，程序会打包成  `sever/target/einvoice-1.0.jar`
